﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioNormal:Envio
    {
        //CORRECCIÓN GENERAL: CALCULARCOSTOENVIO Y CALCULARFECHA DEBEN SER UN ÚNICO MÉTODO Y GUARDAR EL VALOR QUE CALCULAN EN LAS RESPECTIVAS PROPIEDADES DEL ENVÍO, NO DEBEN RETORNAR NADA.
        public override double CalcularCostoEnvio()
        {
            return ((10.25 * VolumenPaquete) + (5.75 * PesoPaquete));
        }

        public override DateTime CalcularFechaEntrega()
        {
            return FechaIngreso.AddDays(3);
        }
    }
}
