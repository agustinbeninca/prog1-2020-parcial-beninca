﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioCliente
    {
        public DateTime FechaIngreso { get; set; }
        public string NombreCliente { get; set; }
        public bool Entregado { get; set; }

    }
}
