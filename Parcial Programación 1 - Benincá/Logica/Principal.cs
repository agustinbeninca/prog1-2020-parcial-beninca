﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        List<Envio> ListaEnvios = new List<Envio>();
        public DateTime CargarNuevoEnvio(int codigoOrigen, int codigoDestino, double volumen, double peso, string nombreCliente)
        {
            //CORRECCIÓN: NO PODES INSTANCIAR NUNCA UN ENVÍO PORQUE ES ABSTRACTO, SI PODRÍAS INSTANCIAR SUS SUBCLASES (ENVIOFRAGIL, ENVIONORMAL O ENVIOPRIORITARIO) SEGÚN NECESITES.
            Envio nuevoEnvio = new Envio();
            nuevoEnvio.CodigoPostalOrigen = codigoOrigen;
            nuevoEnvio.CodigoPostalDestino = codigoDestino;
            nuevoEnvio.CodigoSeguimiento = nuevoEnvio.GenerarCodigoSeguimiento();
            nuevoEnvio.VolumenPaquete = volumen;
            nuevoEnvio.PesoPaquete = peso;
            nuevoEnvio.FechaIngreso = DateTime.Today;
            nuevoEnvio.FechaEntrega = nuevoEnvio.CalcularFechaEntrega();
            nuevoEnvio.NombreCliente = nombreCliente;
            ListaEnvios.Add(nuevoEnvio);
            return nuevoEnvio.CalcularFechaEntrega();
        }

        public List<EnvioCliente> ObtenerEnvioPorCliente(string nombreCliente)
        {
            List<EnvioCliente> lista = new List<EnvioCliente>();

            //CORRECCIÓN: MAL USADO EL LISTADO DE ENVIOS
            foreach (var item in Envios)
            {
                if (nombreCliente == item.NombreCliente)
                {
                    EnvioCliente nuevo = new EnvioCliente();
                    nuevo.FechaIngreso = item.Ingreso;
                    nuevo.NombreCliente = nombreCliente;
                    bool entregado = false;

                    //CORRECCIÓN: ESTA PROPIEDAD NO EXISTE, ADEMÁS DE QUE SE DEBIA COMPARAR CON LA FECHA DE ENTREGA
                    if (item.Ingreso > DateTime.Today)
                    {
                        entregado = true;
                    }
                    nuevo.Entregado = entregado;

                    lista.Add(nuevo);

                    //CORRECCIÓN: SE DEBE RETORNAR LA LISTA SOLO AL FINAL DEL CICLO.
                    return lista;
                    }
                }

            //CORRECCIÓN: EN LA PRIMERA ITERACIÓN DEL CICLO, DEVOLVERÍAS NULL, ESTA MAL
            return null;
            }

    }
}
