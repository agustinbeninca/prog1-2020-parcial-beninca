﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioPrioritario:Envio
    {
        //CORRECCIÓN GENERAL: CALCULARCOSTOENVIO Y CALCULARFECHA DEBEN SER UN ÚNICO MÉTODO Y GUARDAR EL VALOR QUE CALCULAN EN LAS RESPECTIVAS PROPIEDADES DEL ENVÍO, NO DEBEN RETORNAR NADA.
        public override double CalcularCostoEnvio()
        {
            double costoEnvio = 0;
            if (CodigoPostalDestino==CodigoPostalOrigen)
            {
                costoEnvio = 120;

                //CORRECCIÓN: ESTE IF PUEDE SACARSE AFUERA Y OPTIMIZAR EL CÓDIGO.
                if (PesoPaquete>5)
                {
                    //CORRECCIÓN: SI EL EXCEDENTE DEL PAQUETE ES EN DECIMALES, DEBÍA REDONDEARSE HACIA ARRIBA COMO COMENTAMOS DURANTE EL PARCIAL
                    double excedente = PesoPaquete - 5;
                    return costoEnvio + (excedente * 12.50);
                }
            }
            else
            {
                costoEnvio = 180;
                if (PesoPaquete > 5)
                {
                    double excedente = PesoPaquete - 5;
                    return costoEnvio + (excedente * 12.50);
                }
            }
            return 0;
        }

        public override DateTime CalcularFechaEntrega()
        {
            double excedente = 0;
            if (CodigoPostalDestino==CodigoPostalOrigen)
            {
                if (PesoPaquete>5)
                {
                    excedente = (PesoPaquete - 5);
                    return FechaIngreso.AddDays(excedente);
                }
                else
                {
                    return FechaIngreso;
                }
            }
            else
            {
                if (PesoPaquete > 5)
                {
                    excedente = (PesoPaquete - 5);
                    return FechaIngreso.AddDays(1 + excedente);
                }
                else
                {
                    return FechaIngreso.AddDays(1);
                }
            }
        }
    }
}
