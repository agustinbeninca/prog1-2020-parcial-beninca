﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Envio
    {
        public int CodigoPostalOrigen { get; set; }
        public int CodigoPostalDestino { get; set; }
        public int CodigoSeguimiento { get; set; }
        public double VolumenPaquete { get; set; }
        public double PesoPaquete { get; set; }
        public DateTime FechaIngreso { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string NombreCliente { get; set; }

        //CORRECCIÓN GENERAL: CALCULARCOSTOENVIO Y CALCULARFECHA DEBEN SER UN ÚNICO MÉTODO Y GUARDAR EL VALOR QUE CALCULAN EN LAS RESPECTIVAS PROPIEDADES DEL ENVÍO, NO DEBEN RETORNAR NADA.
        public abstract double CalcularCostoEnvio();
        public abstract DateTime CalcularFechaEntrega();
        public int GenerarCodigoSeguimiento()
        {
            //CORRECCIÓN: ESTE CÓDIGO GENERA NUMEROS ALEATORIOS PERO NO SIEMPRE DE 6 DÍGITOS COMO SE PIDE EN LE ENUNCIADO
            Random codigoSeguimiento = new Random();
            return codigoSeguimiento.Next(00000, 99999);
        }

    }
}
