﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class EnvioFragil:Envio
    {
        //CORRECCIÓN GENERAL: CALCULARCOSTOENVIO Y CALCULARFECHA DEBEN SER UN ÚNICO MÉTODO Y GUARDAR EL VALOR QUE CALCULAN EN LAS RESPECTIVAS PROPIEDADES DEL ENVÍO, NO DEBEN RETORNAR NADA.

        //CORRECCIÓN: ESTO DEBÍA SER UN ENUMERADOR
        public int Tipo { get; set; } //1=liquidos 2=explosivos
        public override double CalcularCostoEnvio()
        {
            double costoEnvio = 0;
            if (Tipo==1)
            {
                costoEnvio = 240;
                if (CodigoPostalOrigen==CodigoPostalDestino)
                {
                    return costoEnvio + ((costoEnvio * 7) / 100);
                }
                else
                {
                    return costoEnvio;
                }
            }
            else
            {
                costoEnvio = 275;

                //CORRECCIÓN: ESTE IF ES IGUAL QUE AL DE ARRIBA, ESO QUIERE DECIR QUE PODRÍAS SACARLO AFUERA DEL PRIMER IF Y OPTIMIZAR EL CÓDIGO.
                if (CodigoPostalOrigen == CodigoPostalDestino)
                {
                    return costoEnvio + ((costoEnvio * 7) / 100);
                }
                else
                {
                    return costoEnvio;
                }
            }
        }

        public override DateTime CalcularFechaEntrega()
        {
            return FechaIngreso.AddDays(5);
        }
    }
}
